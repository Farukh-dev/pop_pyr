<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Get The Industry's Best Quotes | StarClinch</title>

	<link rel="shortcut icon" href="img/logo.png">
	<link rel="icon" type="image/png" href="img/logo.png" sizes="100x100">
	<link rel="apple-touch-icon" sizes="100x100" href="img/logo.png">

	<!-- Bootstraps -->
	<link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/component.progressbutton.css" />
	<link rel="stylesheet" type="text/css" href="css/component.button.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.input.css" />
	<link rel="stylesheet" type="text/css" href="css/component.input.css" />
	<!-- Animate css-->
	<link rel="stylesheet" type="text/css" href="css/animate.min.css" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" />

	<link rel="stylesheet" href="css/themify-icons.min.css" media="all">
	<link rel="stylesheet" id="theme-style-css" href="css/styleshop.clean.min.css" type="text/css" media="all">

	<!-- Date css -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.13.0/themes/prism.min.css">
	<link rel="stylesheet" type="text/css" href="css/pignose.calendar.min.css" />


	<script src="js/modernizr.custom.js"></script>
	<script src="js/modernizr.progress.custom.js"></script>



	<script>
		window['_fs_debug'] = false;
		window['_fs_host'] = 'fullstory.com';
		window['_fs_org'] = 'BR041';
		window['_fs_namespace'] = 'FS';
		(function (m, n, e, t, l, o, g, y) {
			if (e in m) {
				if (m.console && m.console.log) {
					m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');
				}
				return;
			}
			g = m[e] = function (a, b) {
				g.q ? g.q.push([a, b]) : g._api(a, b);
			};
			g.q = [];
			o = n.createElement(t);
			o.async = 1;
			o.src = 'https://' + _fs_host + '/s/fs.js';
			y = n.getElementsByTagName(t)[0];
			y.parentNode.insertBefore(o, y);
			g.identify = function (i, v) {
				g(l, {
					uid: i
				});
				if (v) g(l, v)
			};
			g.setUserVars = function (v) {
				g(l, v)
			};
			g.event = function (i, v) {
				g('event', {
					n: i,
					p: v
				})
			};
			g.shutdown = function () {
				g("rec", !1)
			};
			g.restart = function () {
				g("rec", !0)
			};
			g.consent = function (a) {
				g("consent", !arguments.length || a)
			};
			g.identifyAccount = function (i, v) {
				o = 'account';
				v = v || {};
				v.acctId = i;
				g(o, v)
			};
			g.clearUserCookie = function () {};
		})(window, document, window['_fs_namespace'], 'script', 'user');
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125820108-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-125820108-1');
	</script>

</head>

<body class=" body header-logo-center fixed-header footer-block">

	<div id="headerwrap">
		<header id="header" class="pagewidth clearfix" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
			<div class="back-button">

				<a href="javascript:history.back()"> <i class="fas fa-arrow-left "></i></a>

			</div>

			<a id="menu-icon" href="/#mobile-menu"><span class="menu-icon-inner"></span></a>
			<div class="logo-wrap">
				<div id="site-logo"><a href="/" title="Back to homepage"><img
							src="https://starclinch.com/wp-content/uploads/2019/03/StarClinch-Logo.svg"
							id="logoimagecss"></a></div>
				<div id="site-description" class="site-description"><span>Back to Homepage</span></div>
			</div>

		</header>
	</div>

	<section id="loading" style="margin-top: 20%; margin-bottom: 30%;">
		<div class="row text-center">
			<div class="text-center m-auto">
				<div class="loader"></div>
			</div>
		</div>
	</section>

	<div id="body" class="clearfix" style="padding-top: 67px; display: none;">
		<div id="get-quote" class="container">
			<section class="content" style="padding-top: 20px;">

				<div class="row">
					<div class="col-md-2 col-sm-2 col-sm-offset-0 col-xs-4 col-xs-offset-4">
						<div id="snippet-artist" class="row" style="display: none;">
							<div class="col-md-10 col-sm-12 col-xs-12">
								<div class="thumbnail">
									<img id="snippet-artist-thumb"
										src="http://marketline.com/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png"
										alt="" style="width:100%">
									<div class="caption">
										<p id="snippet-artist-name" class="text-center"></p>
										<p id="snippet-artist-category" class="text-center" style="font-size: 12px;">
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-8 col-sm-8 col-xs-12">
						<h2 id="text-title">What are you looking for?</h2>
						<h4 id="text-subtitle">Get the Best Quotes in the Industry</h4>
					</div>
				</div>



				<fieldset id="fields-category">
					<br>
					<div class="row">
						<div class="col-md-2 col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-liveband'});return false;"
									data-value="category-liveband">
									<img src="img/cat-icon/liveBand.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Live Band</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-singer'});return false;"
									data-value="category-singer">
									<img src="img/cat-icon/singer.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Singer</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-celebrity'});return false;"
									data-value="category-celebrity">
									<img src="img/cat-icon/celebrity.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Celebrity</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-comedian'});return false;"
									data-value="category-comedian">
									<img src="img/cat-icon/comedian.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Comedian</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-anchor'});return false;"
									data-value="category-anchor">
									<img src="img/cat-icon/anchor.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Anchor/Emcee</p>
									</div>
								</a>
							</div>
						</div>

						<div class="col-md-2 col-sm-3 col-xs-4 align-middle">
							<p class="morebutton">More Categories</p>
							<!-- <button class="btn btn-7 btn-7g btn-icon-only icon-plus" data-toggle="collapse" data-target="#collapse-category" id="button-show-more-categories" onclick="$(this).parent().hide();">+ More Categories</button> -->
							<button class="btn btn-7 btn-7g btn-icon-only icon-plus" id="button-show-more-categories"
								onclick="$(this).parent().hide();">+ More Categories</button>
						</div>

						<div id="collapse-category" class="collapse">
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-dj'});return false;"
										data-value="category-dj">
										<img src="img/cat-icon/dj.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">DJ</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-dancer'});return false;"
										data-value="category-dancer">
										<img src="img/cat-icon/dancer.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Dancer</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-insturmentalist'});return false;"
										data-value="category-insturmentalist">
										<img src="img/cat-icon/insturmentalist.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Instrumentalist</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-magician'});return false;"
										data-value="category-magician">
										<img src="img/cat-icon/magician.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Magician</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-makeupartist'});return false;"
										data-value="category-makeupartist">
										<img src="img/cat-icon/makeup.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Makeup Artist</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-model'});return false;"
										data-value="category-model">
										<img src="img/cat-icon/model.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Model</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-photographer'});return false;"
										data-value="category-photographer">
										<img src="img/cat-icon/photographer.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Photographer</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-speaker'});return false;"
										data-value="category-speaker">
										<img src="img/cat-icon/speaker.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Speaker</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-1', 'event_label': 'category-varietyartist'});return false;"
										data-value="category-varietyartist">
										<img src="img/cat-icon/variety.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Variety Artist</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>


				</fieldset>


				<fieldset id="fields-event">
					<br />
					<!-- <div id="snippet-artist" class="row" style="display: none;">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 col-lg-offset-5 col-md-offset-5 col-sm-offset-4 col-xs-offset-3">
							<div class="thumbnail">
						        <img src="img/event-icon/restaurant.jpg" alt="" style="width:100%">
						        <div class="caption">
						          <p id="snippet-artist-title" class="text-center">Artist</p>
						          <p id="snippet-artist-subtitle" class="text-center" style="font-size: 12px;">Category</p>
						        </div>
						    </div>
						</div>
					</div>-->

					<div class="row">
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-wedding'});return false;"
									data-value="event-wedding">
									<img src="img/event-icon/wedding.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Wedding</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-privateparty'});return false;"
									data-value="event-privateparty">
									<img src="img/event-icon/privateParty.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Private Party</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-inauguration'});return false;"
									data-value="event-inauguration">
									<img src="img/event-icon/inauguration.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Inauguration</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-corporate'});return false;"
									data-value="event-corporate">
									<img src="img/event-icon/corporate.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Corporate</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-2 col-sm-3 col-xs-4">
							<div class="thumbnail">
								<a href=""
									onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-campus'});return false;"
									data-value="event-campus">
									<img src="img/event-icon/campus.jpg" alt="" style="width:100%">
									<div class="caption">
										<p class="text-center">Campus</p>
									</div>
								</a>
							</div>
						</div>

						<div class="col-md-2 col-sm-3 col-xs-4 align-middle">
							<p class="morebutton">More Events</p>
							<!-- <button class="btn btn-7 btn-7g btn-icon-only icon-plus" data-toggle="collapse" data-target="#collapse-event" id="button-show-more-events" onclick="$(this).parent().hide();">+ More Events</button> -->
							<button class="btn btn-7 btn-7g btn-icon-only icon-plus" id="button-show-more-events"
								onclick="$(this).parent().hide();">+ More Events</button>
						</div>

						<div id="collapse-event" class="collapse">
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-charity'});return false;"
										data-value="event-charity">
										<img src="img/event-icon/charity.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Charity</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-concert'});return false;"
										data-value="event-concert">
										<img src="img/event-icon/concert.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Concert/Festival</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-exhibition'});return false;"
										data-value="event-exhibition">
										<img src="img/event-icon/exhibition.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Exhibition</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-fashionshow'});return false;"
										data-value="event-fashionshow">
										<img src="img/event-icon/fashionShow.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Fashion Show</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-kidsparty'});return false;"
										data-value="event-kidsparty">
										<img src="img/event-icon/kidsParty.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Kids Party</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-photoshoot'});return false;"
										data-value="event-photoshoot">
										<img src="img/event-icon/photoShoot.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Photo/Video Shoot</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-professionalhiring'});return false;"
										data-value="event-professionalhiring">
										<img src="img/event-icon/professionalHiring.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Professional Hiring</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-religious'});return false;"
										data-value="event-religious">
										<img src="img/event-icon/religious.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Religious</p>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-2 col-sm-3 col-xs-4">
								<div class="thumbnail">
									<a href=""
										onclick="gtag('event', 'click', {'event_category': 'pyr-2', 'event_label': 'event-restaurant'});return false;"
										data-value="event-restaurant">
										<img src="img/event-icon/restaurant.jpg" alt="" style="width:100%">
										<div class="caption">
											<p class="text-center">Restaurant</p>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="row" style="padding-top: 30px;"> -->
					<!-- <div class="col-md-12"> -->
					<!-- <button id="button-previous-event" class="btn btn-7 btn-7g btn7-previous btn-icon-only icon-arrow-left">Previous</button> -->
					<!-- <input id="button-previous-event" type="submit" class="y-button" value="Back"> -->
					<!-- </div> -->
					<!-- </div> -->

				</fieldset>

				<fieldset id="fields-meta">
					<div class="row">
						<div class="col-md-12">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo input-meta" autocomplete="off"
									type="text" placeholder="Event Location" id="input-venue" name="eventvenue"
									data-dependency="input-date" data-validation="required"
									data-validation-error-msg="You did not enter a valid venue address"
									data-validation-error-msg-container="#venue-error-dialog"
									onclick="gtag('event', 'click', {'event_category': 'pyr-3', 'event_label': 'input-location'});" />
								<label class="input__label input__label--hideo" for="input-venue">
									<i class="fa fa-fw fa-map-marker icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Event Location</span>
								</label>
							</span>
						</div>
					</div>
					<div id="venue-error-dialog" class="validation-error"></div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<span class="input input--hideo">
								<input class="input__field  input__field--hideo input-meta" autocomplete="off"
									type="text" placeholder="Event Date" id="input-date" name="datepicker"
									onclick="gtag('event', 'click', {'event_category': 'pyr-3', 'event_label': 'input-date'});" />
								<label class="input__label input__label--hideo" for="input-date">
									<i class="fa fa-fw fa-calendar icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Event Date</span>
								</label>
							</span>
							<div id="date-error-dialog" class="validation-error"></div>
						</div>

						<!-- <div class="col-md-4">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo input-meta" type="number" id="input-gathering" placeholder="Gathering" data-dependency="input-budget" data-validation="required" data-validation-error-msg="You did not enter a valid gathering size" data-validation-error-msg-container="#gathering-error-dialog" />
								<label class="input__label input__label--hideo" for="input-gathering">
									<i class="fa fa-fw fa-users icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Gathering</span>
								</label>
							</span>
							<div id="gathering-error-dialog" class="validation-error"></div>
						</div> -->

						<div class="col-md-6 col-md-offset-3">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo input-meta" autocomplete="off"
									type="number" id="input-budget" placeholder="Approx Budget"
									data-validation="required" data-validation-error-msg="You did not enter your budget"
									data-validation-error-msg-container="#budget-error-dialog"
									onclick="gtag('event', 'click', {'event_category': 'pyr-3', 'event_label': 'input-budget'});" />
								<label class="input__label input__label--hideo" for="input-budget">
									<i class="fa fa-fw fa-inr icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Approx Budget</span>
								</label>
							</span>
							<div id="budget-error-dialog" class="validation-error"></div>
						</div>

					</div>

					<div class="row">
						<!-- <div class="col-md-6 col-sm-6 col-xs-6" style="padding-top: 30px;"> -->
						<!-- <button id="button-previous-meta" class="btn btn-7 btn-7g btn7-previous btn-icon-only icon-arrow-left">Previous</button> -->
						<!-- <input id="button-previous-meta" type="submit" class="y-button" value="Back"> -->

						<!-- </div> -->
						<div class="col-md-12 col-sm-12 col-xs-12" style="padding-top: 30px;">
							<!-- <button id="button-continue-meta" class="btn btn-7 btn-7g btn-icon-only icon-arrow-right">Continue</button> -->
							<input id="button-continue-meta" type="submit" class="y-button" value="Continue"
								onclick="gtag('event', 'click', {'event_category': 'pyr-3', 'event_label': 'continue'});">
						</div>
					</div>
				</fieldset>

				<fieldset id="fields-user">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo input-user" autocomplete="off"
									type="text" id="input-name" placeholder="Full Name" data-validation="required"
									data-validation-error-msg="You did not enter a valid name"
									data-validation-error-msg-container="#name-error-dialog"
									onclick="gtag('event', 'click', {'event_category': 'pyr-4', 'event_label': 'input-name'});" />
								<label class="input__label input__label--hideo" for="input-name">
									<i class="fa fa-fw fa-user icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Full Name</span>
								</label>
							</span>
							<div id="name-error-dialog" class="validation-error"></div>
						</div>

						<div class="col-md-6 col-md-offset-3">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo input-user" autocomplete="off"
									type="email" id="input-email" placeholder="Email" data-validation="email"
									data-validation-error-msg="You did not enter a valid e-mail"
									data-validation-error-msg-container="#email-error-dialog"
									onclick="gtag('event', 'click', {'event_category': 'pyr-4', 'event_label': 'input-email'});" />
								<label class="input__label input__label--hideo" for="input-email">
									<i class="fa fa-fw fa-envelope icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Email</span>
								</label>
							</span>
							<div id="email-error-dialog" class="validation-error"></div>
						</div>

						<div class="col-md-6 col-md-offset-3">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo input-user" autocomplete="off"
									type="number" id="input-phone" placeholder="Phone Number" data-validation="length"
									data-validation-length="min10"
									data-validation-error-msg="You did not enter a valid 10 digit number"
									data-validation-error-msg-container="#phone-error-dialog"
									onclick="gtag('event', 'click', {'event_category': 'pyr-4', 'event_label': 'input-phone'});" />
								<label class="input__label input__label--hideo" for="input-phone">
									<i class="fa fa-fw fa-phone icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Phone Number</span>
								</label>
							</span>
							<div id="phone-error-dialog" class="validation-error"></div>
						</div>

						<div class="col-md-6 col-lg-offset-3 col-md-offset-3">
							<span class="input input--hideo">
								<textarea class="input__field input__field--hideo" type="text" id="input-info"
									placeholder="Additional Info" cols="40" rows="2" style="resize: none;"
									onclick="gtag('event', 'click', {'event_category': 'pyr-4', 'event_label': 'input-additional'});"></textarea>
								<label class="input__label input__label--hideo" for="input-info">
									<i class="fa fa-fw fa-info icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo">Additional details
										you want us to know</span>
								</label>
							</span>
						</div>
					</div>

					<div class="row">
						<!-- <div class="col-md-3 col-sm-3 col-xs-12" style="padding-top: 30px;"> -->
						<!-- <button id="button-previous-user" class="btn btn-7 btn-7g btn7-previous btn-icon-only icon-arrow-left">Previous</button> -->
						<!-- <input id="button-previous-user" type="submit" class="y-button" value="Back"> -->
						<!-- </div> -->
						<div class="col-md-12 col-sm-12 col-sx-12" style="padding-top: 30px;">
							<button id="button-submit" class="user-details-button-submit y-button progress-button"
								onclick="gtag('event', 'click', {'event_category': 'pyr-5', 'event_label': 'get-quote'});"
								data-perspective data-horizontal>Get Quote</button>
						</div>
					</div>
				</fieldset>

				<fieldset id="fields-mobiles">
					<div class="center_div">

						<h4> Please enter the OTP below to submit your request </h4>
						<br />

						<div class="form-group">
							<input name="otp-value" type="number" required="" class="form-control otp-input-field"
								placeholder="Enter OTP" id="mobile-Otp-number"><br><br>
						</div>

						<br>
						<div class="form-group">
							<input type="submit" value="Submit otp" id="button-submit-otp" class="y-button">
						</div>

					</div>

				</fieldset>

				<fieldset id="fields-thanks">
					<h1>Thanks!</h1>
					<br />
					<a href="https://starclinch.com/browse"><button
							class="btn btn-primary continueshop">Continue</button></a>

				</fieldset>


			</section>
		</div>


		<div id="after-get-quote" class="container">
			<section class="content" style="padding-top: 20px;">

				<h2 id="text-title-after">Thanks! Help us act fast by providing more information</h2>
				<h4 id="text-subtitle-after">Which gender do you want?</h4>

				<fieldset id="fields-gender">
					<div class="row">
						<div class="col-lg-offset-1 col-lg-5 col-md-offset-1 col-md-5 col-sm-6 col-xs-6">
							<div class="thumbnail">
								<a href="" onclick="return false;" data-value="gender-female">
									<img src="img/gender-icon/girl.png" alt="" style="height: 200px">
									<div class="caption">
										<p class="text-center">Female</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
							<div class="thumbnail">
								<a href="" onclick="return false;" data-value="gender-male">
									<img src="img/gender-icon/boy.png" alt="" style="height:200px">
									<div class="caption">
										<p class="text-center">Male</p>
									</div>
								</a>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset id="fields-audience">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="thumbnail">
								<a href="" onclick="return false;" data-value="audience-below30">
									<img src="img/audience-icon/less30.png" alt="" style="height: 70px">
									<div class="caption">
										<p class="text-center">Below 30 year olds</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="thumbnail">
								<a href="" onclick="return false;" data-value="audience-both">
									<img src="img/audience-icon/both.png" alt="" style="height:70px">
									<div class="caption">
										<p class="text-center">Both</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="thumbnail">
								<a href="" onclick="return false;" data-value="audience-above30">
									<img src="img/audience-icon/more30.png" alt="" style="height:70px">
									<div class="caption">
										<p class="text-center">Above 30 year olds</p>
									</div>
								</a>
							</div>
						</div>
					</div>
				</fieldset>

				<fieldset id="fields-thanks-last">
					<img src="https://i.giphy.com/media/5ArJanyCfxgiY/giphy.webp" style="width: 70%;">
				</fieldset>
			</section>
		</div>
	</div>

	<br>
	<div id="footerwrap">
		<footer id="footer" class="pagewidth clearfix" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
			<div class="footer-column-wrap clearfix">
				<div class="footer-logo-wrap">
					<div class="footer-text-outer">
						<div class="footer-text clearfix">
							<div class="one">Made with <span style="color:#ff0000">❤</span> from all over the india
							</div>
							<br /><br />
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
	<script type="text/javascript">
		// var themifyScript = {"lightbox":{"lightboxSelector":".themify_lightbox","lightboxOn":true,"lightboxContentImages":false,"lightboxContentImagesSelector":"","theme":"pp_default","social_tools":false,"allow_resize":true,"show_title":false,"overlay_gallery":false,"screenWidthNoLightbox":600,"deeplinking":false,"contentImagesAreas":"","gallerySelector":".gallery-icon > a","lightboxGalleryOn":true},"lightboxContext":"body"};
	</script>

	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBGauyAD765wmqPpYsSo5EBKRREdrSZcDQ">
	</script>

	<script src="js/classie.js"></script>
	<script src="js/progressButton.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.77/jquery.form-validator.min.js">
	</script>

	<script type="text/javascript" src="js/pignose.calendar.full.min.js"></script>

	<!-- Date piker js -->
	<script type="text/javascript">
		$(function () {

			function onApplyHandler(date, context) {
				/**
				 * @date is an array which be included dates(clicked date at first index)
				 * @context is an object which stored calendar interal data.
				 * @context.calendar is a root element reference.
				 * @context.calendar is a calendar element reference.
				 * @context.storage.activeDates is all toggled data, If you use toggle type calendar.
				 * @context.storage.events is all events associated to this date
				 */

				var $element = context.element;
				var $calendar = context.calendar;
				var $box = $element.siblings('.box').show();
				var text = 'You applied date ';

				if (date[0] !== null) {
					text += date[0].format('YYYY-MM-DD');
				}

				if (date[0] !== null && date[1] !== null) {
					text += ' ~ ';
				} else if (date[0] === null && date[1] == null) {
					text += 'nothing';
				}

				if (date[1] !== null) {
					text += date[1].format('YYYY-MM-DD');
				}

				$box.text(text);
			}

			// Input Calendar
			var minDate = moment().set('dates', Math.min(moment().day(), 2 + 1)).format('YYYY-MM-DD');
			var maxDate = moment().set('dates', Math.max(moment().day(), 24 + 1)).format('YYYY-MM-DD');
			$('#input-date').pignoseCalendar({
				apply: onApplyHandler,
				//buttons: true, // It means you can give bottom button controller to the modal which be opened when you click.
				minDate: minDate,
				format: 'DD MMM, YYYY',
				//format: 'ddd, MMM DD, YYYY'
			});

			// Color theme type Calendar
			$('.calendar-dark').pignoseCalendar({
				theme: 'dark', // light, dark, blue
				select: onSelectHandler
			});

			// Blue theme type Calendar
			$('.calendar-blue').pignoseCalendar({
				theme: 'blue', // light, dark, blue
				select: onSelectHandler
			});

			// This use for DEMO page tab component.
			$('.menu .item').tab();
		});
		//]]>
	</script>


	<script type="text/javascript">
		// google places API
		var defaultBounds = new google.maps.LatLngBounds(
			new google.maps.LatLng(4.511943, 64.809355),
			new google.maps.LatLng(38.952500, 96.758838));
		// whole of india preffered

		var input = document.getElementById('input-venue');

		var options = {
			bounds: defaultBounds,
			types: ['establishment', 'geocode']
		};

		autocomplete = new google.maps.places.Autocomplete(input, options);
		autocomplete.addListener('place_changed', fillInAddress);

		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			//console.log(place);

			//for (var component in componentForm) {
			//    document.getElementById(component).value = '';
			//    document.getElementById(component).disabled = false;
			//}

			//// Get each component of the address from the place details
			//// and fill the corresponding field on the form.
			//for (var i = 0; i < place.address_components.length; i++) {
			//    var addressType = place.address_components[i].types[0];
			//    if (componentForm[addressType]) {
			//        var val = place.address_components[i][componentForm[addressType]];
			//        //console.log(val);
			//        //document.getElementById(addressType).value = val;
			//    }
			//}
		}

		var buttons7Click = Array.prototype.slice.call(document.querySelectorAll('#btn-click button'));

		buttons7Click.forEach(function (el, i) {
			el.addEventListener('click', activate, false);
		});

		function activate() {
			var self = this,
				activatedClass = 'btn-activated';
			if (!classie.has(this, activatedClass)) {
				classie.add(this, activatedClass);
				setTimeout(function () {
					classie.remove(self, activatedClass)
				}, 1000);
			}
		}

		// $(window).on('popstate', function (e) {
		//     var state = e.originalEvent.state;
		//     if (state !== null) {
		//         //load content with ajax
		//     }
		// });

		$.validate({
			form: '#get-quote',
			validateOnEvent: true
		});
	</script>

	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

	<!-- Custom js -->
	<script src="js/get-quote.js"></script>

	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Animation custom Js -->
	<script>
		//$(".thumbnail").addClass('animated shake');
	</script>


</body>

</html>